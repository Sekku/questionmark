﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace DateLib
{
    /// <summary>
    /// Support class for several operations over dates in string format
    /// </summary>
    public static class DateOperations
    {
        // Regular expression to check whether a string is in ISO8601 format or not
        private const string DateRegexp = @"^(\d{4})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$";
        private static readonly Regex RegExp = new Regex(DateRegexp, RegexOptions.Compiled);

        private static readonly int[] DaysPerMonthNonLeapYear = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        private static readonly int[] DaysPerMonthLeapYear = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        /// <summary>
        /// Returns true if <paramref name="year"/> is leap. Otherwise returns false
        /// </summary>
        /// <param name="year"></param>
        /// <remarks>
        /// Info from https://en.wikipedia.org/wiki/Leap_year#Algorithm
        /// </remarks>
        /// <returns></returns>
        private static bool IsLeapYear(int year)
        {
            if ((year % 400) == 0) return true;
            if ((year % 4) == 0 && (year % 100) != 0) return true;

            return false;
        }

        /// <summary>
        /// Transform a date (divided in year, month and day) into its representation as Julian Day
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="day"></param>
        /// <remarks>
        /// Why in Julian numbers? From the answer to this question https://stackoverflow.com/questions/12862226/the-implementation-of-calculating-the-number-of-days-between-2-dates
        /// Converting a Gregorian date to its representation as Julian number is O(1) and it's easier to calculate the 
        /// difference in days between two dates
        /// The algorithm to get the Julian number is from https://en.wikipedia.org/wiki/Julian_day
        /// </remarks>
        /// <returns></returns>
        private static int GetJulianDayNumber(int year, int month, int day)
        {
            int a = (14 - month) / 12;
            int y = year + 4800 - a;
            int m = month + 12 * a - 3;

            int julianDayNumber = day;
            julianDayNumber += (153 * m + 2) / 5;
            julianDayNumber += 365 * y;
            julianDayNumber += y / 4;
            julianDayNumber -= y / 100;
            julianDayNumber += y / 400;
            julianDayNumber -= 32045;

            return julianDayNumber;
        }

        /// <summary>
        /// Calculates the difference between <paramref name="endDate"/> and <paramref name="startDate"/>
        /// Both parameters are the string representation of a date. 
        /// The format <b>MUST</b> be in ISO8601
        /// </summary>
        /// <param name="startDate">Date as a ISO8601 string</param>
        /// <param name="endDate">Date as a ISO8601 string</param>
        /// <remarks>
        /// More info about ISO8601: https://en.wikipedia.org/wiki/ISO_8601
        /// RFC about dates in the Internet: https://www.ietf.org/rfc/rfc3339.txt
        /// </remarks>
        /// <returns></returns>
        public static int CalculateDaysBetweenDates(string startDate, string endDate)
        {
            // Check first if the dates are null or empty
            if (string.IsNullOrWhiteSpace(startDate))
                throw new ArgumentException($"{nameof(startDate)} cannot be empty", nameof(startDate));
            if (string.IsNullOrWhiteSpace(endDate))
                throw new ArgumentException($"{nameof(endDate)} cannot be empty", nameof(endDate));

            // Next make sure that startDate matches the regular expression of a ISO8601 date
            var startDateMatch = RegExp.Match(startDate);
            if (!startDateMatch.Success)
                throw new ArgumentException($"{nameof(startDate)} should contain a valid date string", nameof(startDate));

            // Check now that the date has the correct day for its month (avoid cases like 2013-02-30)
            int startYear = Int32.Parse(startDateMatch.Groups[1].Value);
            int startMonth = Int32.Parse(startDateMatch.Groups[2].Value);
            int startDay = Int32.Parse(startDateMatch.Groups[3].Value);
            if (IsLeapYear(startYear))
            {
                if (startDay > DaysPerMonthLeapYear[startMonth - 1])
                    throw new ArgumentException($"{nameof(startDate)} should contain a valid date string", nameof(startDate));
            }
            else
            {
                if (startDay > DaysPerMonthNonLeapYear[startMonth - 1])
                    throw new ArgumentException($"{nameof(startDate)} should contain a valid date string", nameof(startDate));
            }

            // Do the same checks for the end date
            var endDateMatch = RegExp.Match(endDate);
            if (!endDateMatch.Success)
                throw new ArgumentException($"{nameof(endDate)} should contain a valid date string", nameof(endDate));

            int endYear = Int32.Parse(endDateMatch.Groups[1].Value);
            int endMonth = Int32.Parse(endDateMatch.Groups[2].Value);
            int endDay = Int32.Parse(endDateMatch.Groups[3].Value);
            if (IsLeapYear(endYear))
            {
                if (endDay > DaysPerMonthLeapYear[endMonth - 1])
                    throw new ArgumentException($"{nameof(endDate)} should contain a valid date string", nameof(endDate));
            }
            else
            {
                if (endDay > DaysPerMonthNonLeapYear[endMonth - 1])
                    throw new ArgumentException($"{nameof(endDate)} should contain a valid date string", nameof(endDate));
            }

            // If both dates are correct, calculate their difference in days as Julian numbers
            return GetJulianDayNumber(endYear, endMonth, endDay) - GetJulianDayNumber(startYear, startMonth, startDay);
        }


        // In order to be more flexible, it would be good to be able to transform a given date in a string and
        // its culture to an ISO8601 format
        [Obsolete("TODO: Implement", true)]
        private static string TransformDateToISO8601(string date, CultureInfo culture)
        {
            throw new NotImplementedException("Not implemented yet");
            // Some cultures (Bulgaria, Turkmenistan) have at the end of the date several symbols (d.M.yyyy 'г.') 
            // that we don't want when trying to convert to ISO8601
            date = date.Remove(date.IndexOf(" '"));
            date = date.Replace(" ", string.Empty);

            string cleanPattern = culture.DateTimeFormat.ShortDatePattern;
            cleanPattern = cleanPattern.Remove(cleanPattern.IndexOf(" '")).Replace(" ", string.Empty);

            // Several cultures (Hungary, Korea, etc...) have a date separator as ". ", with the whitespace
            // so in order to split by only one char, the whitespaces need to be removed
            char sep = culture.DateTimeFormat.DateSeparator.Replace(" ", string.Empty)[0];
            string[] dateParts = date.Split(sep);
            string[] patternParts = cleanPattern.Split(sep);

            string year = string.Empty;
            string month = string.Empty;
            string day = string.Empty;

            for (int i = 0; i < patternParts.Length; i++)
            {
                if (patternParts[i].Contains("y"))
                    year = dateParts[i];
                else if (patternParts[i].Contains("M"))
                    month = dateParts[i];
                else if (patternParts[i].Contains("d"))
                    day = dateParts[i];
            }
            return "";
        }
    }
}