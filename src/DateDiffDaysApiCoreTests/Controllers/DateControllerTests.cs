using Xunit;
using DateDiffDaysApiCore.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DateDiffDaysApiCore.Models;

namespace DateDiffDaysApiCore.Tests
{
    public class DateControllerTests
    {
        [Fact]
        public void IndexShould_ReturnBadRequest_WhenInputIsNull()
        {
            // Arrange
            var controller = new DateController();

            // Act
            var result = controller.Index(null);

            // Assert
            var requestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.IsType<SerializableError>(requestResult.Value);
        }

        [Theory]
        [InlineData("111/22/3333", "1/2/3")]
        [InlineData(null, "")]
        public void IndexShould_ReturnBadRequest_WhenInputIsIncorrect(string startDate, string endDate)
        {
            // Arrange
            var controller = new DateController();

            // Act
            var result = controller.Index(new DateControllerModel() { StartDate = startDate, EndDate = endDate });

            // Assert
            var requestResult = Assert.IsType<BadRequestObjectResult>(result);
            Assert.IsType<SerializableError>(requestResult.Value);
        }

        [Fact]
        public void IndexPostShould_ReturnViewModelValidAndDifferenceInDays_WhenInputIsCorrect()
        {
            // Arrange
            var controller = new DateController();

            // Act
            var result = controller.Index(new DateControllerModel() { StartDate = "2011-02-08", EndDate = "2013-11-25" });

            // Assert
            var requestResult = Assert.IsType<OkObjectResult>(result);
            Assert.IsType<DateControllerModel>(requestResult.Value);
            Assert.True((requestResult.Value as DateControllerModel).DifferenceInDays > 0);
        }
    }
}