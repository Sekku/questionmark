using Xunit;
using DateLib;
using System;

namespace DateLib.Tests
{
    public class DateOperations_DaysBetweenDatesShould
    {
        [Theory]
        [InlineData("", "2013-12-15")]
        [InlineData("2013-12-15", "")]
        [InlineData(null, "2013-12-15")]
        [InlineData("2013-12-15", null)]
        [InlineData("", null)]
        [InlineData(null, "")]
        [InlineData(null, null)]
        [InlineData("", "")]
        public void ThrowIfDatesAreNullOrEmpty(string startDate, string endDate)
        {
            Assert.Throws<ArgumentException>(() => DateOperations.CalculateDaysBetweenDates(startDate, endDate));
        }

        [Theory]
        [InlineData("2013-12-15", "2013-13-15")]
        [InlineData("2013-11-32", "2013-12-15")]
        [InlineData("2013-12-15", "2013-00-15")]
        [InlineData("2013-12-15", "2013-01-00")]
        [InlineData("20132-12-15", "2013-11-10")]
        [InlineData("2013-12-15", "2013-01-100")]
        [InlineData("yyyy-13-15", "2013-12-15")]
        [InlineData("2011-02-29", "2013-01-01")]
        [InlineData("2011-02-22", "2013-06-31")]
        public void ThrowIfDatesAreNotValid(string startDate, string endDate)
        {
            Assert.Throws<ArgumentException>(() => DateOperations.CalculateDaysBetweenDates(startDate, endDate));
        }

        [Theory]
        [InlineData("2013-12-15", "2013-12-15")]
        [InlineData("1984-06-04", "2017-07-11")]
        [InlineData("1582-10-01", "2017-07-11")]
        [InlineData("1582-09-30", "2017-07-11")]
        [InlineData("0001-01-01", "2017-07-11")]
        [InlineData("2017-07-11", "0001-01-01")]
        [InlineData("2012-02-29", "2013-02-28")]
        [InlineData("2011-02-28", "2012-02-29")]
        [InlineData("1984-06-04", "9999-12-31")]
        public void CalculateCorrectlyTheNumberOfDays(string startDate, string endDate)
        {
            var d1 = DateTime.ParseExact(startDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            var d2 = DateTime.ParseExact(endDate, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);

            var off = d2 - d1;

            Assert.Equal<int>(off.Days, DateOperations.CalculateDaysBetweenDates(startDate, endDate));
        }
    }
}