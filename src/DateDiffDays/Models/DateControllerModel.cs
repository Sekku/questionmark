﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DateDiffDays.Models
{
    public class DateControllerModel
    {
        [Required]
        // TODO: Localization
        [MaxLength(10, ErrorMessage = "The maximun length of {0} is {1}")]
        [MinLength(10, ErrorMessage = "The minimun length of {0} is {1}")]
        [DisplayName("Start date")]
        public string StartDate { get; set; }

        [Required]
        // TODO: Localization
        [MaxLength(10, ErrorMessage = "The maximun length of {0} is {1}")]
        [MinLength(10, ErrorMessage = "The minimun length of {0} is {1}")]
        [DisplayName("End date")] 
        public string EndDate { get; set; }

        public int? DifferenceInDays { get; set; }

    }
}