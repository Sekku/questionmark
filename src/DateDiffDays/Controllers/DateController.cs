﻿using DateDiffDays.Models;
using DateLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DateDiffDays.Controllers
{
    public class DateController : Controller
    {
        // GET: Date
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(DateControllerModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    model.DifferenceInDays = DateOperations.CalculateDaysBetweenDates(model.StartDate, model.EndDate);
                }
                catch (ArgumentException ex)
                {
                    // TODO: Localization
                    if (ex.ParamName == "startDate")
                        ModelState.AddModelError("StartDate", $"The value {model.StartDate} is invalid");
                    if (ex.ParamName == "endDate")
                        ModelState.AddModelError("EndDate", $"The value {model.EndDate} is invalid");
                }
                catch (Exception)
                {
                    // TODO: Localization
                    ModelState.AddModelError("UnknownError", "An error occurred while processing your request");
                }
            }

            return View(model);
        }
    }
}