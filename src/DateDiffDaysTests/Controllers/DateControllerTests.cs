﻿using Xunit;
using DateDiffDays.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DateDiffDays.Models;

namespace DateDiffDays.Tests
{
    public class DateControllerTests
    {
        [Fact]
        public void IndexShould_ReturnNotNull()
        {
            // Arrange
            var controller = new DateController();

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.NotNull(result);
        }

        [Theory]
        [InlineData("111/22/3333", "1/2/3")]
        [InlineData(null, "")]
        public void IndexPostShould_ReturnViewAndModelNotValid_WhenInputIsIncorrect(string startDate, string endDate)
        {
            // Arrange
            var controller = new DateController();

            // Act
            var result = controller.Index(new DateControllerModel() { StartDate = startDate, EndDate = endDate }) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.False(result.ViewData.ModelState.IsValid);
        }

        [Fact]
        public void IndexPostShould_ReturnViewModelValidAndDifferenceInDays_WhenInputIsCorrect()
        {
            // Arrange
            var controller = new DateController();

            // Act
            var result = controller.Index(new DateControllerModel() { StartDate = "2011-02-08", EndDate = "2013-11-25" }) as ViewResult;

            // Assert
            Assert.NotNull(result);
            Assert.True(result.ViewData.ModelState.IsValid);
            Assert.True((result.Model as DateControllerModel).DifferenceInDays > 0);
        }
    }
}