using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DateDiffDaysApiCore.Models;
using DateLib;

namespace DateDiffDaysApiCore.Controllers
{
    [Route("api/[controller]")]
    public class DateController : Controller
    {
        [HttpPost]
        // We only accept data in JSON format, not from a form
        public IActionResult Index([FromBody]DateControllerModel model)
        {
            if (model == null)
            {
                return BadRequest(ModelState);
            }

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                model.DifferenceInDays = DateOperations.CalculateDaysBetweenDates(model.StartDate, model.EndDate);
                return Ok(model);
            }
            catch (ArgumentException ex)
            {
                // TODO: Localization
                if (ex.ParamName == "startDate")
                    ModelState.AddModelError("StartDate", $"The value {model.StartDate} is invalid");
                if (ex.ParamName == "endDate")
                    ModelState.AddModelError("EndDate", $"The value {model.EndDate} is invalid");

                return BadRequest(ModelState);
            }
            catch (Exception)
            {
                // TODO: Localization
                ModelState.AddModelError("UnknownError", "An error occurred while processing your request");
                return BadRequest(ModelState);
            }
        }
    }
}