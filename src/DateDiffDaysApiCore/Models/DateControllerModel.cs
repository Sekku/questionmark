using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DateDiffDaysApiCore.Models
{
    public class DateControllerModel
    {
        [Required]
        [MaxLength(10)]
        [MinLength(10)]
        public string StartDate { get; set; }

        [Required]
        [MaxLength(10)]
        [MinLength(10)]
        public string EndDate { get; set; }
        public int? DifferenceInDays { get; set; }
    }
}